package me.mrkirby153.KCNerfer.proxy;

import me.mrkirby153.KCNerfer.voting.VoteHandler;

import java.util.HashMap;

// Server Proxy
public class CommonProxy {



    public boolean isSinglePlayer(){
        return false;
    }

    public void registerRender(){}

    public float getSaturation(){
        throw new RuntimeException("This method should be called on the client!");
    }

    public void setSaturation(float f){
        throw new RuntimeException("This method should be called on the client only.");
    }

    public void setShowSat(boolean b){
        throw new RuntimeException("Called on the wrong side!");
    }

    public boolean getShowSat(){
        throw new RuntimeException("Called on the wrong side!");
    }
    public void setEndVoteTime(VoteHandler.Condition c, long endTime){
        throw new RuntimeException("Called on the wrong side!");
    }
    public HashMap<VoteHandler.Condition, Long> getEndVoteTime(){
        throw new RuntimeException("Called on wrong side!");
    }

    public void setCooldownTime(VoteHandler.Condition c, long cooldownTime){
        throw new RuntimeException("Called on wrong side!");
    }

    public HashMap<VoteHandler.Condition, Long> getCooldownTime(){
        throw new RuntimeException("Called on wrong side!");
    }

    public boolean checkCooldownTimer(VoteHandler.Condition c){
        throw new RuntimeException("Called on wrong side!");
    }

    public void cancelVote(VoteHandler.Condition c){
        throw new RuntimeException("Called on wrong side!");
    }
    public boolean showVotes() {
        throw new RuntimeException("Called on wrong side!");
    }

    public void setShowVotes(boolean showVotes) {
        throw new RuntimeException("Called on wrong side!");
    }
}
