package me.mrkirby153.KCNerfer.proxy;

import cpw.mods.fml.client.registry.RenderingRegistry;
import me.mrkirby153.KCNerfer.poof.PoofEntity;
import me.mrkirby153.KCNerfer.poof.PoofEntityRenderer;
import me.mrkirby153.KCNerfer.voting.VoteHandler;
import net.minecraft.client.Minecraft;

import java.util.HashMap;
import java.util.Iterator;

public class ClientProxy extends CommonProxy {

    private float currentSaturation = -1;
    private boolean showSaturation = false;
    private boolean showVotes = false;
    private HashMap<VoteHandler.Condition, Long> endVoteTime = new HashMap<VoteHandler.Condition, Long>();
    private HashMap<VoteHandler.Condition, Long> cooldownTime = new HashMap<VoteHandler.Condition, Long>();

    @Override
    public boolean isSinglePlayer() {
        return Minecraft.getMinecraft().isSingleplayer();
    }

    @Override
    public void registerRender() {
        RenderingRegistry.registerEntityRenderingHandler(PoofEntity.class, new PoofEntityRenderer());
    }

    @Override
    public float getSaturation() {
        return currentSaturation;
    }

    @Override
    public void setSaturation(float f) {
        this.currentSaturation = f;
    }

    @Override
    public void setShowSat(boolean b) {
        this.showSaturation = b;
    }

    @Override
    public boolean getShowSat() {
        return this.showSaturation;
    }

    @Override
    public void setEndVoteTime(VoteHandler.Condition c, long endTime) {
        endVoteTime.put(c, endTime);
    }

    @Override
    public HashMap<VoteHandler.Condition, Long> getEndVoteTime() {
        return this.endVoteTime;
    }

    @Override
    public void setCooldownTime(VoteHandler.Condition c, long cooldownTime) {
        this.cooldownTime.put(c, cooldownTime);
    }

    @Override
    public HashMap<VoteHandler.Condition, Long> getCooldownTime() {
        return this.cooldownTime;
    }

    @Override
    public boolean checkCooldownTimer(VoteHandler.Condition c) {
        Iterator<VoteHandler.Condition> iterator = cooldownTime.keySet().iterator();
        while (iterator.hasNext()) {
            VoteHandler.Condition con = iterator.next();
            long cooldown = this.cooldownTime.get(con);
            if(cooldown <= System.currentTimeMillis()) {
                iterator.remove();
                return false;
            }
        }
        return true;
    }

    @Override
    public void cancelVote(VoteHandler.Condition c) {
        Iterator<VoteHandler.Condition> iterator = endVoteTime.keySet().iterator();
        while (iterator.hasNext()) {
            VoteHandler.Condition con = iterator.next();
            if (con == c)
                iterator.remove();
        }
    }

    @Override
    public boolean showVotes() {
        return showVotes;
    }

    @Override
    public void setShowVotes(boolean showVotes) {
        this.showVotes = showVotes;
    }
}
