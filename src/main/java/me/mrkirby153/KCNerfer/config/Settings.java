package me.mrkirby153.KCNerfer.config;

import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import me.mrkirby153.KCNerfer.reference.Strings;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

import java.io.File;

public class Settings {

    public static boolean soundEnabled = false;
    public static boolean displaySaturationDefault = false;
    public static boolean displayVotes = false;

    public static int xOffset = 0;
    public static int yOffset = 0;

    public static Configuration config;

    public static void init(File configLocation) {
        config = new Configuration(configLocation);
        FMLCommonHandler.instance().bus().register(new Settings());
    }

    public static void synConfig() {
        Property soundProperty = config.get(Configuration.CATEGORY_GENERAL, "sounds", true);
        soundProperty.comment = "Enable sounds when your saturation is low. Default: true";
        soundEnabled = soundProperty.getBoolean();

        Property yOffsetPorperty = config.get(Configuration.CATEGORY_GENERAL, "yOffset", 0);
        yOffsetPorperty.comment = "Y-offset for the saturation display. Default: 0";
        yOffset = yOffsetPorperty.getInt();

        Property xOffsetProperty = config.get(Configuration.CATEGORY_GENERAL, "xOffset", 0);
        xOffsetProperty.comment = "X-Offset for the saturation display. Default 0";
        xOffset = xOffsetProperty.getInt();

        Property displaySatProp = config.get(Configuration.CATEGORY_GENERAL, "displaySat", false);
        displaySatProp.comment = "Determines if saturation is displayed by default. Default: false";
        displaySaturationDefault = displaySatProp.getBoolean();

        Property showVotesProp = config.get(Configuration.CATEGORY_GENERAL, "showVotes", false);
        showVotesProp.comment = "Show vote status on top of hud";
        displayVotes = showVotesProp.getBoolean();

        if (config.hasChanged())
            config.save();
    }

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent event) {
        if (event.modID.equals(Strings.MODID)) {
            synConfig();
        }
    }
}
