package me.mrkirby153.KCNerfer.gui;

import cpw.mods.fml.client.config.GuiConfig;
import me.mrkirby153.KCNerfer.config.Settings;
import me.mrkirby153.KCNerfer.reference.Strings;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;

public class KCNerferGUIConfig extends GuiConfig {
    @SuppressWarnings("unchecked")
    public KCNerferGUIConfig(GuiScreen parentScreen) {
        super(parentScreen, new ConfigElement(Settings.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), Strings.MODID, "KCnerfer", false, false, GuiConfig.getAbridgedConfigPath(Settings.config.toString()));
    }
}
