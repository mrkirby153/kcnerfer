package me.mrkirby153.KCNerfer.gui;

import me.mrkirby153.KCNerfer.recipie.DisabledItem;
import me.mrkirby153.KCNerfer.recipie.RecipeHandler;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.crafting.IRecipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GuiRecipeDump extends GuiScreen {

    private int currentPage = 1;
    private int totalPages = 0;
    private int ITEMS_PER_PAGE = 2;

    private int totalItems = 0;
    private ArrayList<String> items = new ArrayList<String>();

    private GuiButton prevButton;
    private GuiButton nextButton;

    @Override
    @SuppressWarnings("unchecked")
    public void initGui() {
        //TODO: Fix items per page
        currentPage = 2;
        totalPages = 0;
        totalItems = 0;
        items = new ArrayList<String>();
        ITEMS_PER_PAGE = (int) Math.floor(this.width / 55);
        prevButton = new GuiButton(0, 5, this.height - (20 + 5), 20, 20, "<<");
        if (currentPage == 1)
            prevButton.enabled = false;
        this.buttonList.add(prevButton);
        nextButton = new GuiButton(1, this.width - (5 + 20), this.height - (20 + 5), 20, 20, ">>");
        this.buttonList.add(nextButton);
        HashMap<String, ArrayList<DisabledItem>> dItems = RecipeHandler.disabledItems;
        // Form the items arrayList
        int index = 0;
        for (String playerName : dItems.keySet()) {
            ArrayList<DisabledItem> items = dItems.get(playerName);
            for (DisabledItem d : items) {
                this.items.add(index + ":" + playerName + " - " + d.getModName() + ":" + d.getItemName() + ":" + (d.getAllDataValues() ? "*" : d.getDataValue()));
                index++;
            }
        }
        totalItems = this.items.size();
        totalPages = (int) Math.ceil((float) totalItems / (float) ITEMS_PER_PAGE);
        if (currentPage >= totalPages) {
            currentPage = totalPages;
            nextButton.enabled = false;
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.enabled) {
            if (button.id == 0) {
                currentPage -= 1;
                if (currentPage <= 1) {
                    currentPage = 1;
                    this.prevButton.enabled = false;
                }
                this.nextButton.enabled = true;
            }
            if (button.id == 1) {
                currentPage += 1;
                if (currentPage >= totalPages) {
                    currentPage = totalPages;
                    this.nextButton.enabled = false;
                }
                this.prevButton.enabled = true;
            }
        }
    }

    @Override
    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
        this.drawDefaultBackground();
        // Title
        this.drawCenteredString(this.fontRendererObj, "KCNerfer Recipe Dump", this.width / 2, 5, 0xFFFFFF);
        // Draw pagination numbers
        String paginationString = this.currentPage + " / " + this.totalPages + " (" + this.ITEMS_PER_PAGE + " items per page, " + totalItems + " total)";
        this.drawCenteredString(this.fontRendererObj, paginationString, this.width / 2, this.height - 20, 0xffffff);
        int startIndex = (currentPage < 2) ? 0 : (currentPage - 1) * ITEMS_PER_PAGE + (currentPage - 1);
        System.out.println(startIndex);
        /*
        Page: 1
        StartIndex: 0
        Items Per page: 11
        End index: 11

        Page: 2
        Start Index: 12
        Items per page: 11
        End index: 21

        Page: 3
        SI: 22
        Items per page: 11
        End Index: 33

        Page 4
        SI: 34
        Items per page: 11
        End indexL 45
         */
        int endIndex = startIndex + this.ITEMS_PER_PAGE;
        System.out.println(endIndex);
        ArrayList<String> toDisplay = retrieve(this.items, startIndex, endIndex);
        int currY = 0;
        // Draw the actual items
        for (String s : toDisplay) {
            this.drawCenteredString(this.fontRendererObj, s, 4 + this.fontRendererObj.getStringWidth(s) / 2, currY + 40, 0xffffff);
            currY += 15;
        }
        // Draw a list of wrapped recipes
        ArrayList<IRecipe> allRecipes = retrieveRecipe(RecipeHandler.getRecipieList(), startIndex, endIndex);
        currY = 0;
        for (IRecipe r : allRecipes) {
            String tD = r.getRecipeOutput().getUnlocalizedName() + ":" + r.getRecipeOutput().getItemDamage();
            this.drawCenteredString(this.fontRendererObj, tD, this.width - this.fontRendererObj.getStringWidth(tD) / 2 - 4, currY + 40, 0xffffff);
            currY += 15;
        }

        super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
    }

    @Override
    public boolean doesGuiPauseGame() {
        return true;
    }

    //TODO: Fix broken retrieve method
    //TODO: Fix pagination
    private ArrayList<String> retrieve(ArrayList<String> array, int startIndex, int endIndex) {
        ArrayList<String> returnArray = new ArrayList<String>();
        if (array.size() < endIndex)
            endIndex = (array.size() - 1);
        for (int i = startIndex; i <= endIndex; i++) {
            returnArray.add(array.get(i));
        }
        return returnArray;
    }

    private ArrayList<IRecipe> retrieveRecipe(List<IRecipe> array, int startIndex, int endIndex) {
        ArrayList<IRecipe> returnArray = new ArrayList<IRecipe>();
        if (array.size() < endIndex)
            endIndex = array.size();
        for (int i = startIndex; i < endIndex; i++) {
            returnArray.add(array.get(i));
        }
        return returnArray;
    }
}
