package me.mrkirby153.KCNerfer.log;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;
import net.minecraftforge.common.MinecraftForge;

public class AdminLog {

    private String playerName;
    private int messageCooldown = 0;

    private boolean logsEnabled = true;
    private int logLevel = 1;

    public AdminLog(String playerName) {
        this.playerName = playerName;
        MinecraftForge.EVENT_BUS.register(this);
        FMLCommonHandler.instance().bus().register(this);
    }


    public void sendLogMessage(Level logLevel, String message) {
        if (MinecraftServer.getServer() == null) {
            return;
        }
        if (MinecraftServer.getServer().getConfigurationManager() == null)
            return;
        EntityPlayer player = MinecraftServer.getServer().getConfigurationManager().func_152612_a(playerName);
        if (player == null)
            return;
        // Always log critical errors
        IChatComponent component;
        if (logLevel == Level.CRITICAL) {
            String criticalString = StatCollector.translateToLocal("chat.adminlog.critical").replace("%message%", message);
            component = IChatComponent.Serializer.func_150699_a(criticalString);
            player.addChatComponentMessage(component);
            player.playSound("note.pling", 1F, 1F);
            return;
        }
        if (logsEnabled) {
            // Determine the level of messages
            int msgLevel = logLevel.level;
            Level level = Level.findLevel(msgLevel);
            String unlocalizedKey = "chat.adminlog." + level.name().toLowerCase();
            if (!StatCollector.canTranslate(unlocalizedKey)) {
                component = new ChatComponentText(unlocalizedKey);
            } else {
                component = IChatComponent.Serializer.func_150699_a(StatCollector.translateToLocal(unlocalizedKey).replace("%message%", message));
            }
            if (msgLevel >= this.logLevel)
                player.addChatComponentMessage(component);
        }
    }

    public void sendLogMessage(String message) {
        sendLogMessage(Level.INFO, message);
    }


    public void sendCooldownMessage(String message) {
        if (messageCooldown > 0 || !logsEnabled)
            return;
        messageCooldown = 20 * 10;
        sendLogMessage(Level.DEBUG, message);
    }

    @SubscribeEvent
    public void onTick(TickEvent.ServerTickEvent event) {
        if (event.phase == TickEvent.Phase.START) {
            if (messageCooldown > 0)
                messageCooldown--;
        }
    }

    public void setLog(boolean state) {
        this.logsEnabled = state;
    }

    public boolean isLogActive() {
        return this.logsEnabled;
    }

    public int getLogLevel() {
        return this.logLevel;
    }

    public void setLevel(Level newLevel) {
        this.logLevel = newLevel.level;
    }

    public void test() {
        int currentLevel = this.logLevel;
        boolean active = this.logsEnabled;
        this.logLevel = Level.DEBUG.level;
        this.logsEnabled = true;
        sendLogMessage(Level.DEBUG, "Testing debug messages");
        sendLogMessage(Level.INFO, "Testing info messages");
        sendLogMessage(Level.SEVERE, "Testing severe messages");
        sendLogMessage(Level.CRITICAL, "Testing critical messages");
        this.logLevel = currentLevel;
        this.logsEnabled = active;
    }


    public static enum Level {
        DEBUG(0),
        INFO(1),
        SEVERE(2),
        CRITICAL(3);

        private final int level;

        Level(int level) {
            this.level = level;
        }

        public static Level findLevel(int level) {
            for (Level l : Level.class.getEnumConstants()) {
                if (l.level == level)
                    return l;
            }
            return DEBUG;
        }
    }
}
