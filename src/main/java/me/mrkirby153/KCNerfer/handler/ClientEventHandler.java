package me.mrkirby153.KCNerfer.handler;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.config.Settings;
import me.mrkirby153.KCNerfer.playTime.PlayTimeData;
import me.mrkirby153.KCNerfer.recipie.RecipeHandler;
import me.mrkirby153.KCNerfer.voting.VoteHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.ServerChatEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class ClientEventHandler {
    @SubscribeEvent
    public void onClientTick(TickEvent.ClientTickEvent event) {
        drawCapes();
    }

    @SubscribeEvent
    public void debugScreen(RenderGameOverlayEvent.Text event) {
        // Debug screen
        if (Minecraft.getMinecraft().gameSettings.showDebugInfo) {
            // Show debug info
            EntityPlayer player = Minecraft.getMinecraft().thePlayer;
            ItemStack item = player.getHeldItem();
            event.left.add("");
            event.left.add("Is Disabled: " + ((item == null) ? "false" : RecipeHandler.isDisabled(item, player)) + " OSSP: " + KCNerfer.overrideSSP + ", isDev: " + KCNerfer.isDev);
            event.left.add("Time to idle: " + PlayTimeData.get(player).timeToIdle());
            if (item == null) {
                return;
            }
            GameRegistry.UniqueIdentifier identifier = GameRegistry.findUniqueIdentifierFor(item.getItem());
            event.left.add(identifier.modId + ":" + identifier.name + ":" + item.getItemDamage());
        }
        // Ingame HUD
        if (Minecraft.getMinecraft() != null) {
            if (KCNerfer.proxy.getShowSat()) {
                FontRenderer renderer = Minecraft.getMinecraft().fontRenderer;
                int xPos = (event.resolution.getScaledWidth() / 2) - 3;
                int yPos = event.resolution.getScaledHeight() - 50;
                if (Minecraft.getMinecraft().thePlayer == null)
                    return;
                if (Minecraft.getMinecraft().thePlayer.getFoodStats() == null)
                    return;
                int saturation = Math.round(KCNerfer.proxy.getSaturation());
                final int fullSaturation = 0x00FF00;
                final int halfSaturation = 0xFFA200;
                final int dangerSaturation = 0xFF0000;
                int toDisplay = 0;
                if (saturation >= 15)
                    toDisplay = fullSaturation;
                if (saturation < 15 && saturation >= 5)
                    toDisplay = halfSaturation;
                if (saturation < 5)
                    toDisplay = dangerSaturation;
                if (saturation >= 10)
                    xPos -= 2;
                renderer.drawStringWithShadow(Integer.toString(saturation), xPos + Settings.xOffset, yPos + Settings.yOffset, toDisplay);
            }
            if(KCNerfer.proxy.showVotes()) {
                // Show active votes
                FontRenderer renderer = Minecraft.getMinecraft().fontRenderer;
                int xPos = (event.resolution.getScaledWidth() / 2);
                int yPos = 3;
                int offset = 0;
                HashMap<VoteHandler.Condition, Long> endVoteTime = KCNerfer.proxy.getEndVoteTime();
                ArrayList<VoteHandler.Condition> displayedAlready = new ArrayList<VoteHandler.Condition>();
                //TODO: Fix cooldown timer
                for (VoteHandler.Condition c : endVoteTime.keySet()) {
                    // Calculate minutes
                    long milis = endVoteTime.get(c) - System.currentTimeMillis();
                    long mins = TimeUnit.MILLISECONDS.toMinutes(milis);
                    long secs = TimeUnit.MILLISECONDS.toSeconds(milis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milis));
                    String formatetedMins = "";
                    if (mins < 10) {
                        formatetedMins += "0" + mins;
                    } else {
                        formatetedMins += mins;
                    }
                    formatetedMins += ":";
                    if (secs < 10)
                        formatetedMins += "0" + secs;
                    else
                        formatetedMins += secs;
                    String toDraw = c.toString() + " - " + formatetedMins;
                    renderer.drawStringWithShadow(toDraw, xPos - (renderer.getStringWidth(toDraw) / 2), yPos + offset, 0x00FF00);
                    offset += 10;
                    displayedAlready.add(c);
                }
                // Show Cooldown votes
                HashMap<VoteHandler.Condition, Long> cooldownTime = KCNerfer.proxy.getCooldownTime();
                for (VoteHandler.Condition c : cooldownTime.keySet()) {
                    if (KCNerfer.proxy.checkCooldownTimer(c)) {
                        if (displayedAlready.contains(c))
                            continue;
                        // Calculate minutes
                        long milis = cooldownTime.get(c) - System.currentTimeMillis();
                        long mins = TimeUnit.MILLISECONDS.toMinutes(milis);
                        long secs = TimeUnit.MILLISECONDS.toSeconds(milis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milis));
                        String formatetedMins = "";
                        if (mins < 10) {
                            formatetedMins += "0" + mins;
                        } else {
                            formatetedMins += mins;
                        }
                        formatetedMins += ":";
                        if (secs < 10)
                            formatetedMins += "0" + secs;
                        else
                            formatetedMins += secs;
                        String toDraw = c.toString() + " - " + formatetedMins;
                        renderer.drawStringWithShadow(toDraw, xPos - (renderer.getStringWidth(toDraw) / 2), yPos + offset, 0x737373);
                        offset += 10;
                    }
                }
            }
        }

    }

    @SubscribeEvent
    public void onChat(ServerChatEvent event) {
        if (event.message.equalsIgnoreCase("!kcsat")) {
            boolean currentSatStat = KCNerfer.proxy.getShowSat();
            KCNerfer.proxy.setShowSat(!currentSatStat);
            event.player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_GREEN + "Toggled display of saturation to " + !currentSatStat));
            event.setCanceled(true);
        }
    }

    private void drawCapes() {
        Minecraft mc = Minecraft.getMinecraft();
        if (mc.theWorld != null) {
            for (int i = 0; i < mc.theWorld.playerEntities.size(); i++) {
                AbstractClientPlayer player = (AbstractClientPlayer) mc.theWorld.playerEntities.get(i);
                if (!player.getDisplayName().equalsIgnoreCase("mrkirby153")) {
                    continue;
                }
                //System.out.println("Drawing cape for mrkirby153");
                player.func_152121_a(MinecraftProfileTexture.Type.CAPE, new ResourceLocation("kcnerfer:mrkirby153.png"));
            }
        }
    }
}
