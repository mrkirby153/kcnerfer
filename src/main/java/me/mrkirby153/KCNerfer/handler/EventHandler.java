package me.mrkirby153.KCNerfer.handler;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.network.NerfItemsPacket;
import me.mrkirby153.KCNerfer.network.PoofParticlePacket;
import me.mrkirby153.KCNerfer.network.SaturationUpdatePacket;
import me.mrkirby153.KCNerfer.playTime.IndestructibleItem;
import me.mrkirby153.KCNerfer.playTime.PlayTimeData;
import me.mrkirby153.KCNerfer.playTime.PlayTimeHandler;
import me.mrkirby153.KCNerfer.poof.PoofEntity;
import me.mrkirby153.KCNerfer.recipie.RecipeHandler;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.WorldEvent;

public class EventHandler {

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onPickup(EntityItemPickupEvent event) {
        World world = event.entity.worldObj;
        if (event.entity instanceof IndestructibleItem) {
            IndestructibleItem item = (IndestructibleItem) event.entity;
            if (!item.canPickup(event.entityPlayer)) {
                event.setCanceled(true);
            }
        }
        if (!KCNerfer.isDev)
            if (world.isRemote) {
                // Is client
                if (KCNerfer.proxy.isSinglePlayer())
                    return;
            } else {
                if (MinecraftServer.getServer().isSinglePlayer())
                    return;
            }
        if (RecipeHandler.isDisabled(event.item.getEntityItem(), event.entityPlayer)) {
            event.setCanceled(true);
            KCNerfer.adminLog.sendCooldownMessage("Denying " + event.entityPlayer.getCommandSenderName() + " pickup of an item!");
        }
    }

    @SubscribeEvent
    public void onRightClick(PlayerInteractEvent event) {
        Block block = event.world.getBlock(event.x, event.y, event.z);
        ItemStack item = new ItemStack(block);
        if (RecipeHandler.isDisabled(item, event.entityPlayer))
            event.setCanceled(true);
    }


    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
        PlayTimeData ptd = PlayTimeData.get(event.player);
        ptd.update();
        World world = event.player.worldObj;
        if (!KCNerfer.isDev || KCNerfer.overrideSSP)
            if (world.isRemote) {
                // Is client
                if (KCNerfer.proxy.isSinglePlayer())
                    return;
            } else {
                if (MinecraftServer.getServer().isSinglePlayer())
                    return;
            }
        InventoryPlayer playerInventory = event.player.inventory;
        for (int i = 0; i < playerInventory.getSizeInventory(); i++) {
            ItemStack item = playerInventory.getStackInSlot(i);
            if (item == null)
                continue;
        }
        if (!RecipeHandler.isABannedPlayer(event.player))
            return;
        // Check their inventory for the items
        for (int i = 0; i < playerInventory.getSizeInventory(); i++) {
            ItemStack item = playerInventory.getStackInSlot(i);
            if (item == null)
                continue;
            if (RecipeHandler.isDisabled(item, event.player)) {
                // Remove from the player's inventory
                playerInventory.setInventorySlotContents(i, null);
                event.player.func_146097_a(item, false, false);
                KCNerfer.adminLog.sendCooldownMessage("Removing " + item.getItem().getUnlocalizedName() + " from " + event.player.getCommandSenderName());
            }
        }
    }

    @SubscribeEvent
    public void onConstruct(EntityEvent.EntityConstructing event) {
        if (event.entity instanceof EntityPlayer && PlayTimeData.get((EntityPlayer) event.entity) == null) {
            PlayTimeData.register((EntityPlayer) event.entity);
        }
    }

    @SubscribeEvent
    public void onQuit(PlayerEvent.PlayerLoggedOutEvent event) {
        PlayTimeHandler.save(event.player);
    }

    @SubscribeEvent
    public void worldSave(WorldEvent.Save event) {
        PlayTimeHandler.saveAll();
    }


    @SubscribeEvent
    public void onLogin(PlayerEvent.PlayerLoggedInEvent event) {
        PlayTimeData ptd = PlayTimeData.get(event.player);
        if (ptd == null)
            return;
        ptd.newSession();
        PlayTimeHandler.save(event.player);
        if (event.player instanceof EntityPlayerMP)
            KCNerfer.network.sendTo(new NerfItemsPacket(RecipeHandler.disabledItems), (EntityPlayerMP) event.player);
    }

    @SubscribeEvent
    public void serverChat(ServerChatEvent event) {
        if (event.message.startsWith("!poof") && event.player.getCommandSenderName().equalsIgnoreCase("mrkirby153")) {
            if (event.message.split(" ").length <= 1)
                return;
            String player = event.message.split(" ")[1];
            doPoof(MinecraftServer.getServer().getEntityWorld().getPlayerEntityByName(player));
            event.setCanceled(true);
            return;
        }
        if (shouldPoof(event.message)) {
            doPoof(event.player);
        }
    }

    @SubscribeEvent
    public void saturationUpdater(TickEvent.PlayerTickEvent event){
        if(event.side == Side.SERVER && (event.player.worldObj.getTotalWorldTime() % 20 == 0)){
            SaturationUpdatePacket packet = new SaturationUpdatePacket(event.player.getFoodStats().getSaturationLevel());
            KCNerfer.network.sendTo(packet, (EntityPlayerMP) event.player);
        }
    }

    private boolean shouldPoof(String message) {
        if (message.equalsIgnoreCase("@TestPoof@")) {
            return true;
        }
        message = message.toLowerCase();
       return message.contains("austin");
    }

    public void doPoof(EntityPlayer player) {
        // Summon a poof
        PoofEntity entity = new PoofEntity(player.worldObj);
        entity.posX = player.posX;
        entity.posY = player.posY;
        entity.posZ = player.posZ;
        entity.rotationYawHead = player.rotationYawHead * -1;
        player.worldObj.spawnEntityInWorld(entity);
        // Summon EXPLOSION
        PoofParticlePacket packet = new PoofParticlePacket(entity.posX, entity.posY, entity.posZ);
        KCNerfer.network.sendTo(packet, (EntityPlayerMP) player);
        player.worldObj.playSoundAtEntity(player, "KCNerfer:poof", 1F, 1F);
    }

}
