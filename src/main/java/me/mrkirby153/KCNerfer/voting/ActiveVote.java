package me.mrkirby153.KCNerfer.voting;

import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.network.VoteStatusChangePacket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import java.util.Calendar;
import java.util.HashMap;

public class ActiveVote {

    private String starter;
    private VoteHandler.Condition condition;
    private World forWorld;

    private boolean toDelete = false;

    private long endTime;

    private HashMap<String, VoteHandler.Type> votes = new HashMap<String, VoteHandler.Type>();

    public ActiveVote(VoteHandler.Condition condition, EntityPlayer starter, World forWorld) {
        this.condition = condition;
        this.starter = starter.getCommandSenderName();
        this.forWorld = forWorld;
        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, 30);
        endTime = c.getTimeInMillis();
        VoteStatusChangePacket packet = new VoteStatusChangePacket(VoteStatusChangePacket.Status.CREATE_VOTE, condition, endTime);
        KCNerfer.network.sendToAll(packet);
    }

    public boolean vote(EntityPlayer player, VoteHandler.Type type) {
        if (votes.containsKey(player.getCommandSenderName())) {
            return false;
        }
        votes.put(player.getCommandSenderName(), type);
        return true;
    }


    public boolean recast(EntityPlayer player, VoteHandler.Type type) {
        if (!votes.containsKey(player.getCommandSenderName())) {
            return false;
        }
        votes.remove(player.getCommandSenderName());
        votes.put(player.getCommandSenderName(), type);
        return true;
    }

    public void doAction() {
        // Does the action
        VoteStatusChangePacket packet = new VoteStatusChangePacket(VoteStatusChangePacket.Status.REMOVE, condition);
        KCNerfer.network.sendToAll(packet);
        switch (condition) {
            case RAIN:
                forWorld.getWorldInfo().setThundering(false);
                forWorld.getWorldInfo().setRaining(true);
                break;
            case SUN:
                forWorld.getWorldInfo().setRaining(false);
                break;
            case THUNDER:
                forWorld.getWorldInfo().setRaining(true);
                forWorld.getWorldInfo().setThundering(true);
                break;
            case NIGHT:
                if (!forWorld.isRemote)
                    forWorld.setWorldTime(18000);
                break;
            case DAY:
                if (!forWorld.isRemote)
                    forWorld.setWorldTime(0);
                break;
        }
    }

    public boolean hasVoted(EntityPlayer player) {
        return votes.containsKey(player.getCommandSenderName());
    }

    public String getStarter() {
        return this.starter;
    }

    public VoteHandler.Condition getType() {
        return condition;
    }

    public void update() {
        if (this.endTime <= System.currentTimeMillis())
            KCNerfer.voteHandler.finishVote(this);
    }

    public int getYes() {
        int yesCount = 0;
        for (String players : votes.keySet()) {
            if (votes.get(players) == VoteHandler.Type.YES)
                yesCount++;
        }
        return yesCount;
    }

    public int getNo() {
        int yesCount = 0;
        for (String players : votes.keySet()) {
            if (votes.get(players) == VoteHandler.Type.NO)
                yesCount++;
        }
        return yesCount;
    }

    public boolean toDelete() {
        return toDelete;
    }

    public void markForDeletion() {
        toDelete = true;
    }
}
