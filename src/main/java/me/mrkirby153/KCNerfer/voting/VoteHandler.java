package me.mrkirby153.KCNerfer.voting;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.network.VoteStatusChangePacket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

public class VoteHandler {

    private ArrayList<ActiveVote> activeVotes = new ArrayList<ActiveVote>();
    private HashMap<Condition, Long> typeCooldown = new HashMap<Condition, Long>();

    public boolean castVote(EntityPlayer player, Type type, int voteId) {
        if (activeVotes.size() < voteId)
            return false;
        if (activeVotes.get(voteId) == null)
            return false;
        ActiveVote vote = activeVotes.remove(voteId);
        if (vote.hasVoted(player)) {
            vote.recast(player, type);
        }
        vote.vote(player, type);
        activeVotes.add(vote);

        return true;
    }

    public boolean newVote(EntityPlayer owner, Condition c, World forWorld) {
        if (hasActiveVote(owner))
            return false;
        ActiveVote vote = new ActiveVote(c, owner, forWorld);
        vote.vote(owner, Type.YES);
        activeVotes.add(vote);
        // Send a vote notification to everyone
        ChatComponentText header = new ChatComponentText(EnumChatFormatting.GOLD + "" + EnumChatFormatting.BOLD + "========================================");
        ChatComponentText message = new ChatComponentText(EnumChatFormatting.RED + " " + owner.getCommandSenderName() + " wants to change " + forWorld.getWorldInfo().getWorldName() + "'s state to " + c.name());
        ChatComponentText voteHeader = new ChatComponentText(EnumChatFormatting.AQUA + " Click the options to cast your vote!");
        ChatComponentText expireMessage = new ChatComponentText(EnumChatFormatting.GOLD + " This expires in 30 seconds");
        String voteBar = StatCollector.translateToLocal("chat.vote.voteChoices");
        voteBar = voteBar.replaceAll("%voteid%", Integer.toString(getVoteId(vote)));
        IChatComponent voteBarJson = IChatComponent.Serializer.func_150699_a(voteBar);
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(header);
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(message);
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(voteHeader);
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(expireMessage);
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(voteBarJson);
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(header);
        // Register cooldown
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, 20);
        typeCooldown.put(c, cal.getTimeInMillis());
        VoteStatusChangePacket packet = new VoteStatusChangePacket(VoteStatusChangePacket.Status.SET_COOLDOWN, c, cal.getTimeInMillis());
        KCNerfer.network.sendToAll(packet);
        return true;
    }

    public boolean hasActiveVote(EntityPlayer player) {
        for (ActiveVote votes : activeVotes) {
            if (votes.getStarter().equalsIgnoreCase(player.getCommandSenderName()))
                return true;
        }
        return false;
    }

    private int getVoteId(ActiveVote vote) {
        for (int i = 0; i < activeVotes.size(); i++) {
            ActiveVote v = activeVotes.get(i);
            if (v == vote)
                return i;
        }
        return -1;
    }

    public void cancelVote(EntityPlayer player) {
        for (ActiveVote vote : activeVotes) {
            if (vote.getStarter().equalsIgnoreCase(player.getCommandSenderName())) {
                vote.markForDeletion();
                MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new ChatComponentText(EnumChatFormatting.BOLD + "" + EnumChatFormatting.RED + player.getCommandSenderName() + " has canceled his vote for " + vote.getType().name()));
                VoteStatusChangePacket packet = new VoteStatusChangePacket(VoteStatusChangePacket.Status.REMOVE, vote.getType());
                KCNerfer.network.sendToAll(packet);
            }
        }
    }

    @SubscribeEvent
    public void onServerTick(TickEvent.ServerTickEvent event) {
        Iterator<ActiveVote> voteIterator = activeVotes.iterator();
        while (voteIterator.hasNext()) {
            ActiveVote v = voteIterator.next();
            v.update();
            if (v.toDelete()) {
                voteIterator.remove();
            }
        }
        // Tick Cooldown
        Iterator<Condition> typeIterator = typeCooldown.keySet().iterator();
        while(typeIterator.hasNext()){
            Condition c = typeIterator.next();
            long time = typeCooldown.get(c);
            if(time <= System.currentTimeMillis()){
                MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new ChatComponentText(EnumChatFormatting.GREEN+c.name()+" can now be voted on again!"));
                typeIterator.remove();
            }
        }
    }

    public boolean onCooldown(Condition c) {
        if (typeCooldown.get(c) == null)
            return false;
        long time = typeCooldown.get(c);
        return time <= System.currentTimeMillis();
    }

    public void finishVote(ActiveVote vote) {
        ChatComponentText endedText = new ChatComponentText(EnumChatFormatting.GOLD + "THE VOTING HAS ENDED!");
        ChatComponentText state;
        int yesCount = vote.getYes();
        int noCount = vote.getNo();
        if (yesCount >= noCount) {
            vote.doAction();
            state = new ChatComponentText(EnumChatFormatting.GREEN + "Changing the world state to " + vote.getType().name());
        } else {
            state = new ChatComponentText(EnumChatFormatting.RED + "The world state will be kept the same");
        }
        vote.markForDeletion();
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(endedText);
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(state);
        VoteStatusChangePacket statusChangePacket = new VoteStatusChangePacket(VoteStatusChangePacket.Status.REMOVE, vote.getType());
        KCNerfer.network.sendToAll(statusChangePacket);
    }

    public static enum Type {
        YES,
        NO
    }

    public static enum Condition {
        RAIN,
        SUN,
        THUNDER,
        NIGHT,
        DAY
    }
}
