package me.mrkirby153.KCNerfer.voting;

import me.mrkirby153.KCNerfer.KCNerfer;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class VoteComand extends CommandBase {
    @Override
    public String getCommandName() {
        return "kvote";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/kvote <day/night/rain/clear>";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        // Start a new vote
        if (!(sender instanceof EntityPlayer)) {
            sender.addChatMessage(new ChatComponentText("This command is only executable by the player!"));
            return;
        }
        EntityPlayer player = (EntityPlayer) sender;
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("cancel")) {
                KCNerfer.voteHandler.cancelVote(player);
                return;
            }
            VoteHandler.Condition type;
            try {
                type = VoteHandler.Condition.valueOf(args[0].toUpperCase());
            } catch (Exception e) {
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "That isn't a valid state to vote for"));
                return;
            }
            if(KCNerfer.voteHandler.onCooldown(type)){
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED+"This type is on cooldown"));
                return;
            }
            if (KCNerfer.voteHandler.hasActiveVote(player)) {
                player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.RED + "You already have an active vote! Wait for it to expire first!"));
                player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.RED + "Type /kvote cancel to cancel your vote"));
                return;
            }
            // Create the vote
            KCNerfer.voteHandler.newVote(player, type, player.worldObj);
            player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.RED + "Type /kvote cancel to cancel your vote"));
        }
        if (args.length == 2) {
            int voteNumber;
            try {
                voteNumber = Integer.parseInt(args[0]);
            } catch (Exception e) {
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Incorrect usage!"));
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + getCommandUsage(sender)));
                return;
            }
            VoteHandler.Type type = VoteHandler.Type.valueOf(args[1]);
            if (KCNerfer.voteHandler.castVote(player, type, voteNumber)) {
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Your vote has been recorded!"));
            } else {
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "There was an error processing your vote!"));
            }
        }
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_) {
        return true;
    }
}
