package me.mrkirby153.KCNerfer;

import com.google.common.base.Throwables;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.relauncher.ReflectionHelper;
import cpw.mods.fml.relauncher.Side;
import me.mrkirby153.KCNerfer.command.*;
import me.mrkirby153.KCNerfer.config.Settings;
import me.mrkirby153.KCNerfer.handler.ClientEventHandler;
import me.mrkirby153.KCNerfer.handler.EventHandler;
import me.mrkirby153.KCNerfer.log.AdminLog;
import me.mrkirby153.KCNerfer.network.*;
import me.mrkirby153.KCNerfer.playTime.PlayTimeHandler;
import me.mrkirby153.KCNerfer.poof.PoofEntity;
import me.mrkirby153.KCNerfer.proxy.CommonProxy;
import me.mrkirby153.KCNerfer.recipie.*;
import me.mrkirby153.KCNerfer.reference.Strings;
import me.mrkirby153.KCNerfer.voting.VoteComand;
import me.mrkirby153.KCNerfer.voting.VoteHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.*;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.List;

@Mod(modid = Strings.MODID, version = Strings.VERSION, name = "KirbyCraft Nerfer", guiFactory = "me.mrkirby153.KCNerfer.gui.KCNerferGUIFactory")
public class KCNerfer {

    public static boolean isDev = false;
    public static boolean overrideSSP = false;


    public static Logger logger;

    @SidedProxy(clientSide = "me.mrkirby153.KCNerfer.proxy.ClientProxy", serverSide = "me.mrkirby153.KCNerfer.proxy.CommonProxy")
    public static CommonProxy proxy;

    public static SimpleNetworkWrapper network;

    public static VoteHandler voteHandler;

    public static AdminLog adminLog;

    @Mod.Instance(Strings.MODID)
    public static KCNerfer instance;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        network = NetworkRegistry.INSTANCE.newSimpleChannel("KcNerfer");
        network.registerMessage(NerfItemsPacket.Handler.class, NerfItemsPacket.class, 0, Side.CLIENT);
        network.registerMessage(PoofParticlePacket.Handler.class, PoofParticlePacket.class, 1, Side.CLIENT);
        network.registerMessage(SaturationUpdatePacket.Handler.class, SaturationUpdatePacket.class, 2, Side.CLIENT);
        network.registerMessage(DumpRecipiesPacket.Handler.class, DumpRecipiesPacket.class, 3, Side.CLIENT);
        network.registerMessage(VoteStatusChangePacket.Handler.class, VoteStatusChangePacket.class, 4, Side.CLIENT);
        logger = LogManager.getLogger("KCNerfer");
        adminLog = new AdminLog("mrkirby153");
        KCNerfer.logger.info("");
        isDev = (Boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
        if (isDev) {
            KCNerfer.logger.info("Detected development environment!");
        } else {
            KCNerfer.logger.info("Not running in development environment");
        }
        Settings.init(event.getSuggestedConfigurationFile());
        Settings.synConfig();
        if(event.getSide().isClient()){
            proxy.setShowSat(Settings.displaySaturationDefault);
            proxy.setShowVotes(Settings.displayVotes);
        }
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        EventHandler eh = new EventHandler();
        FMLCommonHandler.instance().bus().register(eh);
        FMLCommonHandler.instance().bus().register(voteHandler = new VoteHandler());
        MinecraftForge.EVENT_BUS.register(eh);

        if (event.getSide().isClient()) {
            ClientEventHandler ceh = new ClientEventHandler();
            MinecraftForge.EVENT_BUS.register(ceh);
            FMLCommonHandler.instance().bus().register(ceh);
        }
        RecipeHandler.init();
        proxy.registerRender();
        EntityRegistry.registerModEntity(PoofEntity.class, "PoofEntity", 0, this, 80, 1, true);
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        runRecipieRemover();
    }

    @Mod.EventHandler
    public void serverLoad(FMLServerStartingEvent event) {
        PlayTimeHandler.loadNBT();
        event.registerServerCommand(new CommandNerfReload());
        event.registerServerCommand(new CommandNerfHand());
        event.registerServerCommand(new CommandPlayTime());
        event.registerServerCommand(new CommandBarfInventory());
        event.registerServerCommand(new VoteComand());
        event.registerServerCommand(new CommandAdminLog());
        event.registerServerCommand(new CommandRecipeDump());
    }

    @SuppressWarnings("unchecked")
    public static void runRecipieRemover() {
        KCNerfer.logger.info("---- RUNNING RECIPE REMOVER -----");
        long startTime = System.currentTimeMillis();
        List<IRecipe> recipeList = CraftingManager.getInstance().getRecipeList();
        for (int i = 0; i < recipeList.size(); i++) {
            IRecipe recipe = recipeList.get(i);
            if (recipe.getRecipeOutput() == null)
                continue;
            if (RecipeHandler.isDisabled(recipe.getRecipeOutput())) {
                IRecipe r = recipe;
                if (recipe instanceof ShapedRecipes) {
                    ShapedRecipes rec = (ShapedRecipes) recipe;
                    r = new CustomShapedRecipe(rec);
                }
                if (recipe instanceof ShapedOreRecipe) {
                    ShapedOreRecipe rec = (ShapedOreRecipe) recipe;
                    r = new CustomOreRecipe(rec);
                }
                if (recipe instanceof ShapelessOreRecipe) {
                    ShapelessOreRecipe rec = (ShapelessOreRecipe) recipe;
                    r = new CustomShapelessOreRecipe(rec);
                }
                if (recipe instanceof ShapelessRecipes) {
                    ShapelessRecipes rec = (ShapelessRecipes) recipe;
                    r = new CustomShapelessRecipe(rec);
                }
                recipeList.set(i, r);
                RecipeHandler.registerRecipie(r);
            }
        }
        long endTime = System.currentTimeMillis();
        KCNerfer.logger.info("----- DONE -----");
        KCNerfer.logger.info("Finished in " + (endTime - startTime) + " miliseconds!");
    }

    private static String eventHandlerFieldName;
    private static String containerThePlayerName;
    private static String slotCraftingThePlayerName;
    private static Field eventHandlerField;
    private static Field containerPlayerPlayerField;
    private static Field slotCraftingPlayerField;

    public static EntityPlayer findPlayer(InventoryCrafting inv) {
        if (eventHandlerFieldName == null)
            eventHandlerFieldName = (KCNerfer.isDev) ? "eventHandler" : "field_70465_c";
        if (containerThePlayerName == null)
            containerThePlayerName = (KCNerfer.isDev) ? "thePlayer" : "field_82862_h";
        if (slotCraftingThePlayerName == null)
            slotCraftingThePlayerName = (KCNerfer.isDev) ? "thePlayer" : "field_75238_b";
        if (eventHandlerField == null)
            eventHandlerField = ReflectionHelper.findField(InventoryCrafting.class, eventHandlerFieldName);
        if (containerPlayerPlayerField == null)
            containerPlayerPlayerField = ReflectionHelper.findField(ContainerPlayer.class, containerThePlayerName);
        if (slotCraftingPlayerField == null)
            slotCraftingPlayerField = ReflectionHelper.findField(SlotCrafting.class, slotCraftingThePlayerName);
        try {
            Container container = (Container) eventHandlerField.get(inv);
            if (container instanceof ContainerPlayer) {
                return (EntityPlayer) containerPlayerPlayerField.get(container);
            } else if (container instanceof ContainerWorkbench) {
                return (EntityPlayer) slotCraftingPlayerField.get(container.getSlot(0));
            } else {
                // don't know the player
                return null;
            }
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }
}
