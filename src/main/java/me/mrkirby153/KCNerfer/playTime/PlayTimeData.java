package me.mrkirby153.KCNerfer.playTime;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class PlayTimeData implements IExtendedEntityProperties {

    public static final String EXT_PROPERTY_NAME = "kc_PlayTime";

    private final EntityPlayer player;

    private long playedTicks;
    private long idleTicks;
    private long sessionTicks;

    private final int IDLE_MINUTES = 5; // 5
    private final int IDLE_TICKS = 20 * (IDLE_MINUTES * 60);
    private int ticksBeforeIdle = IDLE_TICKS;

    private int lastX, lastZ;
    private boolean hasIdleTimerPassed = false;
    private boolean hasMessageBeenShown = false;


    public PlayTimeData(EntityPlayer player) {
        this.player = player;
        this.playedTicks = 0;
    }

    public static void register(EntityPlayer player) {
        if (player.getExtendedProperties(EXT_PROPERTY_NAME) == null)
            player.registerExtendedProperties(EXT_PROPERTY_NAME, new PlayTimeData(player));
    }

    public static PlayTimeData get(EntityPlayer player) {
        if (player == null) return null;
        return (PlayTimeData) player.getExtendedProperties(EXT_PROPERTY_NAME);
    }


    @Override
    public void saveNBTData(NBTTagCompound compound) {
        compound.setLong("playTime", (this.playedTicks));
        compound.setLong("idleTime", (this.idleTicks));
    }

    @Override
    public void loadNBTData(NBTTagCompound compound) {
        playedTicks = compound.getLong("playTime");
        idleTicks = compound.getLong("idleTime");
    }

    @Override
    public void init(Entity entity, World world) {

    }

    public long getPlayedTicks() {
        return this.playedTicks;
    }

    public long getIdleTicks() {
        return this.idleTicks;
    }

    public void setPlayedTicks(long ticks) {
        this.playedTicks = ticks;
    }

    public void setIdleTicks(long ticks) {
        this.idleTicks = ticks;
    }

    public void update() {
        sessionTicks++;
        boolean notMoving = !hasMoved();
        if (notMoving) {
            tickIdleTimer();
        } else {
            resetIdleTimer();
        }
        if (isIdle()) {
            this.idleTicks += 1;
        } else {
            this.playedTicks += 1;
        }
    }

    public boolean isIdle() {
        return hasIdleTimerPassed && !hasMoved();
    }

    public boolean hasMoved() {
        boolean hasMoved = (Math.floor(player.posX) != lastX || Math.floor(player.posZ) != lastZ);
        lastX = (int) Math.floor(player.posX);
        lastZ = (int) Math.floor(player.posZ);
        return hasMoved;
    }

    public void tickIdleTimer() {
        if (hasIdleTimerPassed)
            return;
        if (ticksBeforeIdle <= 0 && !hasMessageBeenShown) {
            hasIdleTimerPassed = true;
            if (MinecraftServer.getServer() != null) {
                MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new ChatComponentText(EnumChatFormatting.GOLD + player.getCommandSenderName() + " is now AFK"));
            }
            this.hasMessageBeenShown = true;
            return;
        }
        ticksBeforeIdle -= 1;
    }

    public void resetIdleTimer() {
        this.ticksBeforeIdle = this.IDLE_TICKS;
        this.hasIdleTimerPassed = false;
        if (hasMessageBeenShown) {
            if (MinecraftServer.getServer() != null) {
                MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new ChatComponentText(EnumChatFormatting.GOLD + player.getCommandSenderName() + " is no longer AFK"));
            }
            this.hasMessageBeenShown = false;
        }
    }

    public long timeToIdle() {
        return ticksBeforeIdle;
    }

    public long currentSession() {
        return sessionTicks;
    }

    public void newSession() {
        sessionTicks = 0;
    }
}
