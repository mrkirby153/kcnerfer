package me.mrkirby153.KCNerfer.playTime;

import me.mrkirby153.KCNerfer.KCNerfer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.DimensionManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class PlayTimeHandler {

    private static NBTTagCompound data = new NBTTagCompound();
    private static int REVISION = 2;

    public static void saveAll() {
        if (KCNerfer.isDev)
            KCNerfer.logger.info("Writing all online playtime to file");
        List l = MinecraftServer.getServer().getConfigurationManager().playerEntityList;
        for (Object playerMP : l) {
            if (playerMP instanceof EntityPlayerMP) {
                save((EntityPlayerMP) playerMP);
            }
        }
    }

    public static void reset(EntityPlayer player) {
        PlayTimeData ptd = PlayTimeData.get(player);
        if (ptd == null)
            return;
        ptd.setPlayedTicks(0);
        ptd.setIdleTicks(0);
        save(player);
    }

    public static void save(EntityPlayer player) {
        if (KCNerfer.isDev)
            KCNerfer.logger.info("Writing for " + player.getCommandSenderName() + "...");
        PlayTimeData ptd = PlayTimeData.get(player);
        if (ptd == null) {
            PlayTimeData.register(player);
            return;
        }
        long playTime = ptd.getPlayedTicks();
        update(player.getCommandSenderName(), playTime, ptd.getIdleTicks());
    }

    private static void update(String playerName, long newTime, long newIdleTime) {
        boolean exists = data.hasKey(playerName);
        NBTTagCompound compound = data.getCompoundTag(playerName);
        compound.setLong("playtime", newTime);
        compound.setLong("idletime", newIdleTime);
        if (compound.getInteger("revision") < REVISION && exists) {
            EntityPlayerMP entityPlayerMP = MinecraftServer.getServer().getConfigurationManager().func_152612_a(playerName);
            PlayTimeData ptd = PlayTimeData.get(entityPlayerMP);
            if(ptd == null)
                return;
            ptd.setIdleTicks(0);
            ptd.setPlayedTicks(0);
            System.out.println("Set player's playtime to zero");
            compound.setInteger("revision", REVISION);
        }
        data.setTag(playerName, compound);
        saveNBT();
    }


    public static void loadNBT() {
        try {
            if (DimensionManager.getCurrentSaveRootDirectory() == null) {
                return;
            }
            File nbtFile = new File(DimensionManager.getCurrentSaveRootDirectory().getAbsolutePath() + File.separator + "playtime.dat");
            if (!nbtFile.exists()) {
                new File(nbtFile.getParent()).mkdirs();
                nbtFile.createNewFile();
            }
            data = CompressedStreamTools.readCompressed(new FileInputStream(nbtFile));
            KCNerfer.logger.info("Loaded NBT data");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveNBT() {
        try {
            File nbtFile = new File(DimensionManager.getCurrentSaveRootDirectory().getAbsolutePath() + File.separator + "playtime.dat");
            if (!nbtFile.exists()) {
                new File(nbtFile.getParent()).mkdirs();
                nbtFile.createNewFile();
            }
            CompressedStreamTools.writeCompressed(data, new FileOutputStream(nbtFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static long[] fromFile(String playerName) {
        NBTTagCompound compound = data.getCompoundTag(playerName);
        return new long[]{compound.getLong("playtime"), compound.getLong("idletime")};
    }

    public static long[] fromPlayer(EntityPlayer player) {
        PlayTimeData ptd = PlayTimeData.get(player);
        if (ptd == null)
            return new long[]{-1, -1};
        return new long[]{ptd.getPlayedTicks(), ptd.getIdleTicks()};
    }

    public static long sessionTicks(EntityPlayer player) {
        PlayTimeData ptd = PlayTimeData.get(player);
        if (ptd == null)
            return -1;
        return ptd.currentSession();
    }
}
