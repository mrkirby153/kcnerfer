package me.mrkirby153.KCNerfer.playTime;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class IndestructibleItem extends EntityItem {

    private final EntityPlayer owner;
    private final ItemStack itemStack;

    public IndestructibleItem(EntityPlayer owner, World world, double x, double y, double z, ItemStack itemStack) {
        super(world, x, y, z, itemStack);
        this.isImmuneToFire = true;
        this.owner = owner;
        this.itemStack = itemStack;
    }

    public boolean canPickup(EntityPlayer player) {

        return player.getCommandSenderName().equalsIgnoreCase(this.owner.getCommandSenderName());
    }

    @Override
    public boolean isEntityInvulnerable() {
        return true;
    }

    @Override
    public boolean canRenderOnFire() {
        return false;
    }

    @Override
    public void onUpdate() {
        Block currentBlock = worldObj.getBlock((int) Math.floor(posX), (int) Math.floor(posY), (int) Math.floor(posZ));
        if (currentBlock instanceof BlockLiquid) {
            // TODO: Make it so items float
            EntityItem item = new EntityItem(owner.worldObj, owner.posX, owner.posY, owner.posZ, this.itemStack);
            item.delayBeforeCanPickup = 0;
            owner.worldObj.spawnEntityInWorld(item);
        }
        super.onUpdate();
    }
}
