package me.mrkirby153.KCNerfer.command;

import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.network.NerfItemsPacket;
import me.mrkirby153.KCNerfer.recipie.RecipeHandler;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;

import java.util.List;

public class CommandNerfReload implements ICommand {


    @Override
    public String getCommandName() {
        return "NerfReload";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/nerfreload";
    }

    @Override
    public List getCommandAliases() {
        return null;
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        RecipeHandler.init();
        // Send the updates to the player
        for(Object o : MinecraftServer.getServer().getConfigurationManager().playerEntityList){
            if(o instanceof EntityPlayerMP){
                KCNerfer.logger.info("Dispatched banned items update to "+((EntityPlayerMP) o).getCommandSenderName());
                KCNerfer.network.sendTo(new NerfItemsPacket(RecipeHandler.disabledItems), (EntityPlayerMP) o);
            }
        }
        KCNerfer.runRecipieRemover();
        sender.addChatMessage(new ChatComponentText("Updated banned items!"));
        KCNerfer.adminLog.sendLogMessage("Updated banned item list!");
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_) {
        return true;
    }

    @Override
    public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
        return false;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
