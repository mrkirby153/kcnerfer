package me.mrkirby153.KCNerfer.command;

import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.log.AdminLog;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class CommandAdminLog extends CommandBase {
    @Override
    public String getCommandName() {
        return "adminlog";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/adminlog";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 0) {
            if (!KCNerfer.adminLog.isLogActive()) {
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "Logging is disabled"));
                return;
            }
            sender.addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "Logging is enabled at lvl " + AdminLog.Level.findLevel(KCNerfer.adminLog.getLogLevel()).name()));
        }
        if(args.length == 1){
            if(args[0].equalsIgnoreCase("toggle")){
                KCNerfer.adminLog.setLog(!KCNerfer.adminLog.isLogActive());
            }
            if(args[0].equalsIgnoreCase("test")){
                KCNerfer.adminLog.test();
            }
        }
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("set")) {
                AdminLog.Level newLevel;
                try {
                    newLevel = AdminLog.Level.valueOf(args[1].toUpperCase());
                } catch (Exception e) {
                    sender.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_PURPLE + "That isn't a log level!"));
                    return;
                }
                KCNerfer.adminLog.setLevel(newLevel);
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Log level set successfully!"));
            }
        }
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return sender.getCommandSenderName().equalsIgnoreCase("mrkirby153");
    }
}
