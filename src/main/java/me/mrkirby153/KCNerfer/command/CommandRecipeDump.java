package me.mrkirby153.KCNerfer.command;

import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.network.DumpRecipiesPacket;
import me.mrkirby153.KCNerfer.recipie.RecipeHandler;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;

public class CommandRecipeDump extends CommandBase{
    @Override
    public String getCommandName() {
        return "kcdump";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/kcdump";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if(sender instanceof EntityPlayer){
            EntityPlayer player = (EntityPlayer) sender;
            KCNerfer.logger.debug("Dispatched dump recipes packet");
            KCNerfer.network.sendTo(new DumpRecipiesPacket(RecipeHandler.disabledItems), (EntityPlayerMP) player);
        } else {
            sender.addChatMessage(new ChatComponentText("You must be a player to use this command!"));
        }
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_) {
        return true;
    }
}
