package me.mrkirby153.KCNerfer.command;

import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.playTime.IndestructibleItem;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CommandBarfInventory extends CommandBase {

    private List aliases = new ArrayList();
    private ArrayList<String> barfInventoryBlacklist = new ArrayList<String>();

    public CommandBarfInventory() {
        aliases.add("BarfInventory");
        barfInventoryBlacklist.add("mrkirby153");
        barfInventoryBlacklist.add("nba_n");
    }

    @Override
    public String getCommandName() {
        return "barfinventory";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "/BarfInventory <Player Name>";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 0) {
            if (sender instanceof EntityPlayer) {
                barfInventory((EntityPlayer) sender);
                ((EntityPlayer) sender).addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Your inventory has been barfed!"));
                return;
            }
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("__test")) {
                EntityPlayer player = (EntityPlayer) sender;
                Random r = new Random();
                EntityItem toSpawnItem = new IndestructibleItem(player, player.worldObj, player.posX + r.nextDouble(), player.posY + (r.nextDouble() * r.nextDouble()), player.posZ + r.nextDouble(), new ItemStack(Blocks.stone));
                toSpawnItem.delayBeforeCanPickup = 20 * 5;
                player.worldObj.spawnEntityInWorld(toSpawnItem);
                return;
            }
            EntityPlayerMP player = getPlayer(sender, args[0]);
            if (player == null) {
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "That player cannot be found!"));
                return;
            }
            if (!barfInventoryBlacklist.contains(args[0].toLowerCase())) {
                barfInventory(player);
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + args[0] + "'s inventory is now on the floor :D"));
                return;
            } else {
                sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "You can not barf that player!"));
                return;
            }
        }
        sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + getCommandUsage(sender)));
    }

    public List getAliases() {
        return this.aliases;
    }

    private void barfInventory(EntityPlayer player) {
        KCNerfer.adminLog.sendLogMessage("Barfing "+player.getCommandSenderName()+"'s inventory");
        InventoryPlayer inventory = player.inventory;
        for (int i = 0; i < inventory.getSizeInventory(); i++) {
            final ItemStack currentItem = inventory.getStackInSlot(i);
            if (currentItem == null)
                continue;
            ItemStack toDrop = currentItem.copy();
            Random r = new Random();
            EntityItem toSpawnItem = new IndestructibleItem(player, player.worldObj, player.posX + r.nextDouble(), player.posY + (r.nextDouble() * r.nextDouble()), player.posZ + r.nextDouble(), toDrop);
            toSpawnItem.delayBeforeCanPickup = 20 * 5;
            player.worldObj.spawnEntityInWorld(toSpawnItem);
            inventory.setInventorySlotContents(i, null);
        }
    }


    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return sender.getCommandSenderName().equalsIgnoreCase("mrkirby153") || sender.getCommandSenderName().equalsIgnoreCase("nba_n");
    }

    @Override
    public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
        return p_71516_2_.length >= 1 ? getListOfStringsMatchingLastWord(p_71516_2_, MinecraftServer.getServer().getAllUsernames()) : null;
    }
}
