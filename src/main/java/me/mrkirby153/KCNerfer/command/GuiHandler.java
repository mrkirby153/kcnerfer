package me.mrkirby153.KCNerfer.command;

import cpw.mods.fml.common.network.IGuiHandler;
import me.mrkirby153.KCNerfer.gui.GuiRecipeDump;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler {
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID){
            case 0:
                return null;
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case 0:
                return new GuiRecipeDump();
        }
        return null;
    }
}
