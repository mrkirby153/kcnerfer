package me.mrkirby153.KCNerfer.recipie;

import me.mrkirby153.KCNerfer.KCNerfer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapedOreRecipe;

import java.lang.reflect.Field;

public class CustomOreRecipe extends ShapedOreRecipe {

    private static Field outputField;
    private static Field inputField;
    private static Field widthField;
    private static Field heightField;
    private static Field mirroredField;

    public CustomOreRecipe(ShapedOreRecipe recipe) {
        super(new ItemStack(Items.stick), "X", "X", "X", 'X', Items.stick);
        try {
            Class shapedRecipieClass = recipe.getClass();
            if (outputField == null)
                outputField = shapedRecipieClass.getDeclaredField("output");
            if (!outputField.isAccessible())
                outputField.setAccessible(true);
            outputField.set(this, outputField.get(recipe));

            if (inputField == null)
                inputField = shapedRecipieClass.getDeclaredField("input");
            if (!inputField.isAccessible())
                inputField.setAccessible(true);
            inputField.set(this, inputField.get(recipe));

            if (widthField == null)
                widthField = shapedRecipieClass.getDeclaredField("width");
            if (!widthField.isAccessible())
                widthField.setAccessible(true);
            widthField.set(this, widthField.get(recipe));

            if (heightField == null)
                heightField = shapedRecipieClass.getDeclaredField("height");
            if (!heightField.isAccessible())
                heightField.setAccessible(true);
            heightField.set(this, heightField.get(recipe));

            if (mirroredField == null)
                mirroredField = shapedRecipieClass.getDeclaredField("mirrored");
            if (!mirroredField.isAccessible())
                mirroredField.setAccessible(true);
            mirroredField.set(this, mirroredField.get(recipe));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public CustomOreRecipe(ItemStack result, Object... recipe) {
        super(result, recipe);
    }

    @Override
    public boolean matches(InventoryCrafting inv, World world) {
        if (!KCNerfer.isDev && !KCNerfer.overrideSSP) {
            if (world == null)
                return super.matches(inv, world);
            if (world.isRemote) {
                if (KCNerfer.proxy.isSinglePlayer()) {
                    return super.matches(inv, world);
                }
            } else {
                if (MinecraftServer.getServer().isSinglePlayer())
                    return super.matches(inv, world);
            }
        }
        // Check the player first
        EntityPlayer player = KCNerfer.findPlayer(inv);
        if (RecipeHandler.isDisabled(super.getRecipeOutput(), player)) {
            return false;
        }
        return super.matches(inv, world);
    }
}
