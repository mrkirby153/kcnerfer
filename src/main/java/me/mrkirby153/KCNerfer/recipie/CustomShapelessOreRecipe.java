package me.mrkirby153.KCNerfer.recipie;

import me.mrkirby153.KCNerfer.KCNerfer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapelessOreRecipe;

import java.lang.reflect.Field;

public class CustomShapelessOreRecipe extends ShapelessOreRecipe {

    private static Field outputField, inputField;
    public CustomShapelessOreRecipe(ShapelessOreRecipe rec) {
        // Pass a garbage constructor. Will be overwritten
        super(new ItemStack(Items.stick), new ItemStack(Items.diamond));

        try{
            Class recipeClass = rec.getClass();
            if(outputField == null)
                outputField = recipeClass.getDeclaredField("output");
            outputField.setAccessible(true);
            outputField.set(this, outputField.get(rec));

            if(inputField == null)
                inputField = recipeClass.getDeclaredField("input");
            inputField.setAccessible(true);
            inputField.set(this, inputField.get(rec));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean matches(InventoryCrafting inv, World world) {
        if (!KCNerfer.isDev && !KCNerfer.overrideSSP) {
            if (world == null)
                return super.matches(inv, world);
            if (world.isRemote) {
                if (KCNerfer.proxy.isSinglePlayer()) {
                    return super.matches(inv, world);
                }
            } else {
                if (MinecraftServer.getServer().isSinglePlayer())
                    return super.matches(inv, world);
            }
        }
        EntityPlayer player = KCNerfer.findPlayer(inv);
        if(player == null)
            return super.matches(inv, world);
        if(RecipeHandler.isDisabled(super.getRecipeOutput(), player)){
            return false;
        }
        return super.matches(inv, world);
    }
}
