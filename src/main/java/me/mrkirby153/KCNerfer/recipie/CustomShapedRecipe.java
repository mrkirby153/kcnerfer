package me.mrkirby153.KCNerfer.recipie;

import me.mrkirby153.KCNerfer.KCNerfer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

import java.lang.reflect.Field;

public class CustomShapedRecipe extends ShapedRecipes {

    private static Field widthField, heightField, itemsField, outputField;

    public CustomShapedRecipe(ShapedRecipes recipe) {
        super(0, 0, null, null);
        try{
            Class shapedRecipieClass = recipe.getClass();
            if(widthField == null)
                widthField = shapedRecipieClass.getDeclaredField((KCNerfer.isDev)? "recipeWidth" : "field_77576_b");
            if(heightField == null)
                heightField = shapedRecipieClass.getDeclaredField((KCNerfer.isDev)? "recipeHeight" : "field_77577_c");
            if(itemsField == null)
                itemsField = shapedRecipieClass.getDeclaredField((KCNerfer.isDev)? "recipeItems" : "field_77574_d");
            if(outputField == null)
                outputField = shapedRecipieClass.getDeclaredField((KCNerfer.isDev)? "recipeOutput" : "field_77575_e");
            widthField.setAccessible(true);
            heightField.setAccessible(true);
            itemsField.setAccessible(true);
            outputField.setAccessible(true);

            widthField.set(this, widthField.get(recipe));
            heightField.set(this, heightField.get(recipe));
            itemsField.set(this, itemsField.get(recipe));
            outputField.set(this, outputField.get(recipe));
        } catch (Exception e){
            e.printStackTrace();
        }
    }



    @Override
    public boolean matches(InventoryCrafting inv, World world) {
        // Check the player first
        if (!KCNerfer.isDev && !KCNerfer.overrideSSP) {
            if (world == null)
                return super.matches(inv, world);
            if (world.isRemote) {
                if (KCNerfer.proxy.isSinglePlayer()) {
                    return super.matches(inv, world);
                }
            } else {
                if (MinecraftServer.getServer().isSinglePlayer())
                    return super.matches(inv, world);
            }
        }
        EntityPlayer player = KCNerfer.findPlayer(inv);
        if(player == null)
            return super.matches(inv, world);
        if(RecipeHandler.isDisabled(super.getRecipeOutput(), player)){
            return false;
        }
        return super.matches(inv, world);
    }

}
