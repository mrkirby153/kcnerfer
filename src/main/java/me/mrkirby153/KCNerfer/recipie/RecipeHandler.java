package me.mrkirby153.KCNerfer.recipie;

import me.mrkirby153.KCNerfer.KCNerfer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RecipeHandler {

    private static final String url = "https://dl.dropboxusercontent.com/u/121954827/modding/KCNerfer/";


    public static HashMap<String, ArrayList<DisabledItem>> disabledItems = new HashMap<String, ArrayList<DisabledItem>>();

    private static ArrayList<IRecipe> recipies = new ArrayList<IRecipe>();

    public static void init() {
        disabledItems = new HashMap<String, ArrayList<DisabledItem>>();
        recipies = new ArrayList<IRecipe>();
        try {
            URL remotePlayerIndex = new URL(url + "players.txt");
            BufferedReader remotePlayerIndexInputStream = new BufferedReader(new InputStreamReader(remotePlayerIndex.openStream()));
            String playerName = "";
            while ((playerName = remotePlayerIndexInputStream.readLine()) != null) {
                if (playerName.startsWith("#"))
                    continue;
                KCNerfer.logger.info("Loading player " + playerName);
                ArrayList<DisabledItem> dItems = new ArrayList<DisabledItem>();
                URL playerFile = new URL(url + playerName + ".txt");
                BufferedReader playerInputStream = new BufferedReader(new InputStreamReader(playerFile.openStream()));
                String item = "";
                while ((item = playerInputStream.readLine()) != null) {
                    if (item.startsWith("#"))
                        continue;
                    // Constuct the new item
                    String[] parts = item.split(":");
                    String modId = parts[0];
                    String itemName = parts[1];
                    int metadata = 0;
                    boolean allValues = false;
                    if (parts[2].equalsIgnoreCase("*")) {
                        allValues = true;
                    } else {
                        metadata = Integer.parseInt(parts[2]);
                    }
                    DisabledItem di;
                    if (allValues)
                        di = new DisabledItem(modId, itemName);
                    else
                        di = new DisabledItem(modId, itemName, metadata);
                    dItems.add(di);
                }
                disabledItems.put(playerName, dItems);
                playerInputStream.close();
            }
            remotePlayerIndexInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isDisabled(ItemStack item, EntityPlayer player) {
        if (player == null || player.getCommandSenderName() == null)
            return false;
        String playerName = player.getCommandSenderName();
        ArrayList<DisabledItem> dItems = disabledItems.get(playerName);
        if (dItems == null)
            return false;
        for (DisabledItem dI : dItems) {
            if (dI.compare(item))
                return true;
        }
        return false;
    }

    public static boolean isDisabled(ItemStack item) {
        for (String s : disabledItems.keySet()) {
            List<DisabledItem> items = disabledItems.get(s);
            if (items.size() == 0)
                return false;
            for (DisabledItem dI : items) {
                if (dI.compare(item))
                    return true;
            }
        }
        return false;
    }

    public static void registerRecipie(IRecipe recipe) {
        recipies.add(recipe);
    }

    public static List<IRecipe> getRecipieList() {
        return recipies;
    }

    public static boolean isABannedPlayer(EntityPlayer player) {
        return disabledItems.containsKey(player.getCommandSenderName());
    }
}
