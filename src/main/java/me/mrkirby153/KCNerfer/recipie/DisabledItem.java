package me.mrkirby153.KCNerfer.recipie;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemStack;

public class DisabledItem {

    private int dataValue;
    private boolean allDataValues;
    private String modName;
    private String itemName;


    public DisabledItem(String modName, String itemName){
        this.modName = modName;
        this.itemName = itemName;
        this.dataValue = 0;
        this.allDataValues = true;
    }

    public DisabledItem(String modName, String itemName, int dataValue){
        this.modName = modName;
        this.itemName = itemName;
        this.dataValue = dataValue;
        this.allDataValues = false;
    }


    public boolean compare(ItemStack itemStack){
        GameRegistry.UniqueIdentifier itemUid = GameRegistry.findUniqueIdentifierFor(itemStack.getItem());
        if(itemUid == null)
            return false;
//        return !(!itemUid.modId.equalsIgnoreCase(this.modName) || !itemUid.name.equalsIgnoreCase(this.itemName) || !this.allDataValues && itemStack.getItemDamage() != dataValue);
        if(this.modName == null || this.itemName == null)
            return false;
        if(!itemUid.modId.equalsIgnoreCase(this.modName))
            return false;
        if(!itemUid.name.equalsIgnoreCase(this.itemName))
            return false;
        if(!this.allDataValues)
            if(itemStack.getItemDamage() != dataValue)
                return false;
        return true;
    }

    public boolean getAllDataValues(){
        return this.allDataValues;
    }

    public int getDataValue(){
        return this.dataValue;
    }

    public String getModName(){
        return this.modName;
    }

    public String getItemName(){
        return this.itemName;
    }
}
