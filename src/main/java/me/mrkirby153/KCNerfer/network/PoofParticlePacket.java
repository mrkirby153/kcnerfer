package me.mrkirby153.KCNerfer.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;

import java.util.Random;

public class PoofParticlePacket implements IMessage {

    public PoofParticlePacket() {

    }

    public double x, y, z;

    public PoofParticlePacket(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        x = buf.readDouble();
        y = buf.readDouble();
        z = buf.readDouble();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeDouble(x);
        buf.writeDouble(y);
        buf.writeDouble(z);
    }

    public static class Handler implements IMessageHandler<PoofParticlePacket, IMessage> {

        @Override
        public IMessage onMessage(PoofParticlePacket message, MessageContext ctx) {
            WorldClient world = Minecraft.getMinecraft().theWorld;
            Random r = world.rand;
            for (int i = 0; i < 1500; i++) {
                world.spawnParticle("explode", message.x + r.nextDouble(), message.y + r.nextDouble() + r.nextInt(1), message.z + r.nextDouble(), 0, 0, 0);
            }
            return null;
        }
    }
}
