package me.mrkirby153.KCNerfer.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.config.Settings;
import net.minecraft.client.Minecraft;

public class SaturationUpdatePacket implements IMessage {

    public SaturationUpdatePacket() {

    }

    public float saturation;
    public boolean playSound;

    public SaturationUpdatePacket(float saturation) {
        this.saturation = saturation;
        this.playSound = (saturation <= 0);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        saturation = buf.readFloat();
        playSound = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeFloat(saturation);
        buf.writeBoolean(playSound);
    }

    public static class Handler implements IMessageHandler<SaturationUpdatePacket, IMessage> {

        @Override
        public IMessage onMessage(SaturationUpdatePacket message, MessageContext ctx) {
            KCNerfer.proxy.setSaturation(message.saturation);
            if (message.playSound && Settings.soundEnabled && KCNerfer.proxy.getShowSat())
                Minecraft.getMinecraft().thePlayer.playSound("note.bass", 1F, 1F);
            return null;
        }
    }
}
