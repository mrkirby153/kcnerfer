package me.mrkirby153.KCNerfer.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.voting.VoteHandler;

public class VoteStatusChangePacket implements IMessage{

    public VoteStatusChangePacket(){

    }

    private Status s = Status.INVALID;
    private VoteHandler.Condition c;
    private long time;
    public VoteStatusChangePacket(Status s, VoteHandler.Condition c){
        if(s != Status.REMOVE){
            throw new RuntimeException("Incorrect usage!");
        }
        this.s = s;
        this.c = c;
    }

    public VoteStatusChangePacket(Status s, VoteHandler.Condition c, long time){
        if(s == Status.REMOVE){
            throw new RuntimeException("Incorrect usage!");
        }
        this.s = s;
        this.c = c;
        this.time = time;
    }
    @Override
    public void fromBytes(ByteBuf buf) {
        String conditionType = ByteBufUtils.readUTF8String(buf);
        Status s = Status.valueOf(conditionType);
        this.s = s;
        switch (s){
            case REMOVE:
                this.c = VoteHandler.Condition.valueOf(ByteBufUtils.readUTF8String(buf));
                break;
            case CREATE_VOTE:
                this.c = VoteHandler.Condition.valueOf(ByteBufUtils.readUTF8String(buf));
                this.time = buf.readLong();
                break;
            case SET_COOLDOWN:
                this.c = VoteHandler.Condition.valueOf(ByteBufUtils.readUTF8String(buf));
                this.time = buf.readLong();
                break;
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        if(this.s == Status.INVALID){
            System.out.println("Ding!");
            for(int i = 0; i < 20; i++){
                KCNerfer.logger.error("====================");
                KCNerfer.logger.error("                    ");
                KCNerfer.logger.error("INVALID STATE. ABORTING");
                KCNerfer.logger.error("                    ");
                KCNerfer.logger.error("====================");
            }
            return;
        }
        // Write the status
        ByteBufUtils.writeUTF8String(buf, this.s.name());
        switch(s){
            case REMOVE:
                ByteBufUtils.writeUTF8String(buf, this.c.name());
                break;
            case CREATE_VOTE:
                // Write Condition
                ByteBufUtils.writeUTF8String(buf, this.c.name());
                // Write timeLeft
                buf.writeLong(this.time);
                break;
            case SET_COOLDOWN:
                // Write Condition
                ByteBufUtils.writeUTF8String(buf, this.c.name());
                // Write cooldown time
                buf.writeLong(this.time);
                break;
        }
    }

    public static class Handler implements IMessageHandler<VoteStatusChangePacket, IMessage>{
        @Override
        public IMessage onMessage(VoteStatusChangePacket message, MessageContext ctx) {
            if(message == null)
                return null;
            switch (message.s){
                case REMOVE:
                    KCNerfer.proxy.cancelVote(message.c);
                    break;
                case CREATE_VOTE:
                    KCNerfer.proxy.setEndVoteTime(message.c, message.time);
                    break;
                case SET_COOLDOWN:
                    KCNerfer.proxy.setCooldownTime(message.c, message.time);
            }
            return null;
        }
    }

    public enum Status{
        REMOVE,
        CREATE_VOTE,
        SET_COOLDOWN,
        INVALID
    }
}
