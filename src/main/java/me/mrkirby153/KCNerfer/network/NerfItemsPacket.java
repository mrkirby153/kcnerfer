package me.mrkirby153.KCNerfer.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import me.mrkirby153.KCNerfer.KCNerfer;
import me.mrkirby153.KCNerfer.recipie.DisabledItem;
import me.mrkirby153.KCNerfer.recipie.RecipeHandler;

import java.util.ArrayList;
import java.util.HashMap;

public class NerfItemsPacket implements IMessage {

    public NerfItemsPacket() {
    }

    public HashMap<String, ArrayList<DisabledItem>> items = new HashMap<String, ArrayList<DisabledItem>>();

    public NerfItemsPacket(HashMap<String, ArrayList<DisabledItem>> items) {
        this.items = items;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        // Now to read them back
        ArrayList<DisabledItem> dItems;
        // Read the total Hash size
        int totalHashSize = buf.readInt();
        for(int i = 0; i < totalHashSize; i++){
            // Read the player's name
            String playerName = ByteBufUtils.readUTF8String(buf);
            // Read the size of the disabled items array
            int disabledItemsSize = buf.readInt();
            dItems = new ArrayList<DisabledItem>();
            for(int j = 0; j < disabledItemsSize; j++){
                // Read the item's data value
                int dataValue = buf.readInt();
                // Read the modid and name combined
                String modIdAndName = ByteBufUtils.readUTF8String(buf);
                // Split apart
                String modId = modIdAndName.split(":")[0];
                String itemName = modIdAndName.split(":")[1];
                // Construct the item
                if(dataValue == -1){
                    DisabledItem disabledItem = new DisabledItem(modId, itemName);
                    dItems.add(disabledItem);
                } else {
                    DisabledItem disabledItem = new DisabledItem(modId, itemName, dataValue);
                    dItems.add(disabledItem);
                }
            }
            this.items.put(playerName, dItems);
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        // Write the total size of the hashmap
        buf.writeInt(items.size());
        // Begin writing the players
        for (String playerName : items.keySet()) {
            // Send the player
            ByteBufUtils.writeUTF8String(buf, playerName);
            // Send the total amount of banned items
            ArrayList<DisabledItem> items = this.items.get(playerName);
            buf.writeInt(items.size());
            // Begin writing the DisabledItems
            for (DisabledItem i : items) {
                // Write the data value or -1 for all the data values
                if(i.getAllDataValues())
                    buf.writeInt(-1);
                else
                    buf.writeInt(i.getDataValue());
                // Write the modName and the itemName
                ByteBufUtils.writeUTF8String(buf, i.getModName()+":"+i.getItemName());
            }
        }
    }

    public static class Handler implements IMessageHandler<NerfItemsPacket, IMessage> {

        @Override
        public IMessage onMessage(NerfItemsPacket message, MessageContext ctx) {
            KCNerfer.logger.info("Received recipes from the server");
            RecipeHandler.disabledItems = message.items;
            return null;
        }
    }
}
