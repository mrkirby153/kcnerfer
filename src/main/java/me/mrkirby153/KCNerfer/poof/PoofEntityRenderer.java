package me.mrkirby153.KCNerfer.poof;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

@SideOnly(Side.CLIENT)
public class PoofEntityRenderer extends RendererLivingEntity {

    private static final ResourceLocation steveTexture = new ResourceLocation("textures/entity/steve.png");

    public PoofEntityRenderer() {
        super(new ModelBiped(0.0F), 0.5F);
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
        // Get the resource location for the skin
        ResourceLocation location = AbstractClientPlayer.getLocationSkin("mrkirby153");
        TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
        if (texturemanager.getTexture(location) == null) {
            AbstractClientPlayer.getDownloadImageSkin(location, "mrkirby153");
        }
        if (texturemanager.getTexture(location) == null)
            return steveTexture;
        else
            return location;
    }
}
