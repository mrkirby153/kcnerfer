package me.mrkirby153.KCNerfer.poof;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class PoofEntity extends EntityLiving {

    private boolean canDie = false;
    private int ticksLived = 0;
    private static int ticksTillDeath = 20 * 10;
    private static int ticksTillDespawn = ticksTillDeath + 20 * 3;


    public PoofEntity(World p_i1595_1_) {
        super(p_i1595_1_);
        this.setHealth(20);
        this.setSize(1, 1.9F);
        this.func_110163_bv();
        this.tasks.addTask(1, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(10.0D);
    }

    @Override
    public boolean canAttackWithItem() {
        return false;
    }

    @Override
    public boolean canAttackClass(Class p_70686_1_) {
        return false;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();
        ticksLived++;
        doDespawn();
    }

    @Override
    public void writeToNBT(NBTTagCompound p_70109_1_) {
        super.writeToNBT(p_70109_1_);
    }

    @Override
    public void readFromNBT(NBTTagCompound p_70020_1_) {
        super.readFromNBT(p_70020_1_);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound p_70014_1_) {
        super.writeEntityToNBT(p_70014_1_);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound p_70037_1_) {
        super.readEntityFromNBT(p_70037_1_);
    }

    @Override
    protected void despawnEntity() {
    }

    @Override
    protected boolean canDespawn() {
        return false;
    }

    @Override
    public void setDead() {
        if (canDie)
            super.setDead();
    }

    public void doDespawn() {
//        System.out.println(ticksLived);
        if (ticksLived >= ticksTillDeath) {
            this.addVelocity(0, 0.1F, 0);
        }
        if(ticksLived >= ticksTillDespawn){
            canDie = true;
            setDead();
        }
    }
}
